import { Shape_construct, Shape_getArea } from './shape.js';
import { Rectangle_construct } from './rectangle.js';


const shape_1 = {};
const shape_2 = {};
const rect_1 = {};
const rect_2 = {};

Shape_construct(shape_1, 1, 1);
Shape_construct(shape_2, 1, 1);
Rectangle_construct(rect_1, 1, 1, 4, 2);
Rectangle_construct(rect_2, 1, 1, 5, 10);

for (let shape of [shape_1, shape_2, rect_1, rect_2]) {
    console.log(
        Shape_getArea(shape)
    );
}
