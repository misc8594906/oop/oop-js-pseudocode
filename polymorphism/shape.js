const Shape = {
    vtable: {
        Shape_getArea: virtual_Shape_getArea
    },

    x: Symbol(),
    y: Symbol(),

};

export function Shape_construct(me, x, y) {
    me.__vtable = Shape.vtable;

    me[Shape.x] = x;
    me[Shape.y] = y;
}

export function Shape_move(me, dx, dy) {
    me[Shape.x] += dx;
    me[Shape.y] += dy;
}

export function Shape_getX(me) {
    return me[Shape.x];
}

export function Shape_getY(me) {
    return me[Shape.y];
}

// virtual
export function Shape_getArea(me) {
    return me.__vtable.Shape_getArea(me);
}

function virtual_Shape_getArea(me) {
    return 0;
}
