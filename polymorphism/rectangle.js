import { Shape_construct } from './shape.js';


const Rectangle = {
    vtable: {
        Shape_getArea: Rectangle_getArea
    },

    width: Symbol(),
    height: Symbol()
};

export function Rectangle_construct(me, x, y, width, height) {
    Shape_construct(me, x, y);

    me.__vtable = Rectangle.vtable;

    me[Rectangle.width] = width;
    me[Rectangle.height] = height;
}

// override
export function Rectangle_getArea(me) {
    return me[Rectangle.width] * me[Rectangle.height];
}
