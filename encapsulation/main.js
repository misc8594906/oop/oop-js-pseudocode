import { Shape_construct, Shape_move, Shape_getX, Shape_getY } from './shape.js';


const shape_1 = {};

Shape_construct(shape_1, 1, 1);
Shape_move(shape_1, 2, 3);

const x = Shape_getX(shape_1);
const y = Shape_getY(shape_1);

console.log(x === 3);
console.log(y === 4);
console.log(shape_1.x);
console.log(shape_1.y);

console.log(Object.getOwnPropertySymbols(shape_1));
