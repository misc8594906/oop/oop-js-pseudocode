// enforce privacy
const getOwnPropertySymbols = Object.getOwnPropertySymbols;
Object.getOwnPropertySymbols = function(obj) {
    const symbols = getOwnPropertySymbols(obj);
    for (const prop in Shape) {
        const index = symbols.indexOf(prop);

        if (index < 0) {
            symbols.splice(index, 1);
        }
    }

    return symbols;
}


const Shape = {
    x: Symbol(),
    y: Symbol()
};

export function Shape_construct(me, x, y) {
    me[Shape.x] = x;
    me[Shape.y] = y;
}

export function Shape_move(me, dx, dy) {
    me[Shape.x] += dx;
    me[Shape.y] += dy;
}

export function Shape_getX(me) {
    return me[Shape.x];
}

export function Shape_getY(me) {
    return me[Shape.y];
}

