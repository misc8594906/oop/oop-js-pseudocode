import { Shape_construct } from './shape.js';


const Rectangle = {
    width: Symbol(),
    height: Symbol()
};

export function Rectangle_construct(me, x, y, width, height) {
    Shape_construct(me, x, y);

    me[Rectangle.width] = width;
    me[Rectangle.height] = height;
}

export function Rectangle_getArea(me) {
    return me[Rectangle.width] * me[Rectangle.height];
}
