import { Shape_construct, Shape_move, Shape_getX, Shape_getY } from './shape.js';
import { Rectangle_construct, Rectangle_getArea } from './rectangle.js';


const shape_1 = {};

Shape_construct(shape_1, 1, 1);
Shape_move(shape_1, 2, 3);

console.log('---- Shape ----');
console.log(Shape_getX(shape_1) === 3);
console.log(Shape_getY(shape_1) === 4);
console.log(shape_1.x);
console.log(shape_1.y);

console.log(Object.getOwnPropertySymbols(shape_1));
console.log('---- End Shape ----\n');

console.log('---- Rectangle ----');
const rectangle_1 = {};

Rectangle_construct(rectangle_1, 2, 2, 5, 6);
console.log(Rectangle_getArea(rectangle_1) === 5 * 6);

console.log(Shape_getX(rectangle_1) === 2);
//console.log(Rectangle_getX(rectangle_1) === 2);

console.log('---- End Rectangle ----\n');

